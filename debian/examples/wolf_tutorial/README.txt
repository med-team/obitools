Original source: https://git.metabarcoding.org/obitools/obitools3/uploads/9b86f67ad05815ddee14526640d81137/wolf_tutorial.tar.gz

Which was derived from https://pythonhosted.org/OBITools/wolves.html ( https://pythonhosted.org/OBITools/_downloads/wolf_tutorial.zip )

Which is a subset from: https://doi.org/10.5061/dryad.443t4m1q
